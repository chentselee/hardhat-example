// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
import { ethers, network } from "hardhat";

async function main() {
  // Hardhat always runs the compile task when running scripts with its command
  // line interface.
  //
  // If this script is run directly using `node` you may want to call compile
  // manually to make sure everything is compiled
  // await hre.run('compile');

  // We get the contract to deploy
  const MyNFT = await ethers.getContractFactory("MyNFT");
  const myNFT = await MyNFT.deploy();
  await myNFT.deployed();
  console.log("MyNFT deployed to:", myNFT.address);
  console.log("name", await myNFT.name());
  console.log("symbol", await myNFT.symbol());

  const receiverAddress = "0xf39Fd6e51aad88F6F4ce6aB8827279cffFb92266";
  // balance: 0
  console.log(await myNFT.balanceOf(receiverAddress));
  const receipt1 = await myNFT.mint(receiverAddress);
  console.log(receipt1);
  // balance: 1
  console.log(await myNFT.balanceOf(receiverAddress));
  console.log(await myNFT.ownerOf(0));

  // block height: 2
  console.log(await network.provider.send("eth_blockNumber"));
  console.log(await network.provider.send("hardhat_mine", ["0x100"]));
  // block height: 258
  console.log(await network.provider.send("eth_blockNumber"));
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
